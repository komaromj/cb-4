%expect 0

%define api.parser.struct {Parser}
%define api.value.type {Value}
%define api.parser.check_debug { self.debug }

%define parse.error custom
%define parse.trace

%code use {
    // all use goes here
    use crate::{Token, C1Lexer as Lexer, Loc, Value};
}

%code parser_fields {
    errors: Vec<String>,
    /// Enables debug printing
    pub debug: bool,
}

%token
    AND           "&&"
    OR            "||"
    EQ            "=="
    NEQ           "!="
    LEQ           "<="
    GEQ           ">="
    LSS           "<"
    GRT           ">"
    KW_BOOLEAN    "bool"
    KW_DO         "do"
    KW_ELSE       "else"
    KW_FLOAT      "float"
    KW_FOR        "for"
    KW_IF         "if"
    KW_INT        "int"
    KW_PRINTF     "printf"
    KW_RETURN     "return"
    KW_VOID       "void"
    KW_WHILE      "while"
    CONST_INT     "integer literal"
    CONST_FLOAT   "float literal"
    CONST_BOOLEAN "boolean literal"
    CONST_STRING  "string literal"
    ID            "identifier"

// definition of association and precedence of operators
%left '+' '-' OR
%left '*' '/' AND
%nonassoc UMINUS

// workaround for handling dangling else
// LOWER_THAN_ELSE stands for a not existing else
%nonassoc LOWER_THAN_ELSE
%nonassoc KW_ELSE

%%

program: 
  %empty { $$ = Value::None }
| program.body program { $$ = Value::None }
;

program.body:
  declassignment ';' { $$ = Value::None }
| functiondefinition { $$ = Value::None }
;

functiondefinition:
  type id '(' parameterlist.opt ')' '{' statementlist '}' { $$ = Value::None }
; 

parameterlist.opt:
  %empty { $$ = Value::None }
| parameterlist { $$ = Value::None }
;

parameterlist: 
  type id parameterlist.more { $$ = Value::None }
;

parameterlist.more:
  %empty { $$ = Value::None }
| ',' type id parameterlist.more { $$ = Value::None }
;

functioncall:
  id '(' functioncall.arguments ')' { $$ = Value::None }
;

functioncall.arguments:
  %empty { $$ = Value::None }
| assignment functioncall.morearguments { $$ = Value::None }
;

functioncall.morearguments:
  %empty { $$ = Value::None }
| ',' assignment functioncall.morearguments { $$ = Value::None }
;

statementlist:
  %empty { $$ = Value::None }
| block statementlist { $$ = Value::None }
;

block:
  '{' statementlist '}' { $$ = Value::None }
| statement { $$ = Value::None }
;

statement:
  ifstatement { $$ = Value::None }
| forstatement { $$ = Value::None }
| whilestatement { $$ = Value::None }
| returnstatement ';' { $$ = Value::None }
| dowhilestatement ';' { $$ = Value::None }
| printf ';' { $$ = Value::None }
| declassignment ';' { $$ = Value::None }
| statassignment ';' { $$ = Value::None }
| functioncall ';' { $$ = Value::None }
;

ifstatement:
  KW_IF '(' assignment ')' block LOWER_THAN_ELSE { $$ = Value::None }
| KW_IF '(' assignment ')' block KW_ELSE block { $$ = Value::None }
;

forstatement:
  KW_FOR '(' statassignment ';' expr ';' statassignment ')' block { $$ = Value::None }
| KW_FOR '(' declassignment ';' expr ';' statassignment ')' block { $$ = Value::None }
;

dowhilestatement:
  KW_DO block KW_WHILE '(' assignment ')' { $$ = Value::None }
;

whilestatement:
  KW_WHILE '(' assignment ')' block { $$ = Value::None }
;

returnstatement:
  KW_RETURN returnstatement.maybe { $$ = Value::None }
;

returnstatement.maybe:
  %empty { $$ = Value::None }
| assignment { $$ = Value::None }
;

printf:
  KW_PRINTF '(' assignment ')' { $$ = Value::None }
| KW_PRINTF '(' CONST_STRING ')' { $$ = Value::None }
;

declassignment:
  type id { $$ = Value::None }
| type statassignment { $$ = Value::None }
;

statassignment:
  id '=' assignment { $$ = Value::None }
;

assignment:
  statassignment { $$ = Value::None }
| expr { $$ = Value::None }
;

expr:
  simpexpr { $$ = Value::None } 
| simpexpr expr.operator simpexpr { $$ = Value::None }
;

expr.operator:
  "==" { $$ = Value::None }
| "!=" { $$ = Value::None }
| "<=" { $$ = Value::None }
| ">=" { $$ = Value::None }
| '<' { $$ = Value::None }
| '>' { $$ = Value::None }
;

simpexpr:
  '-' term simpexpr.operations { $$ = Value::None }
| term simpexpr.operations { $$ = Value::None }
;

simpexpr.operations:
  %empty { $$ = Value::None }
| '+' term simpexpr.operations { $$ = Value::None }
| '-' term simpexpr.operations { $$ = Value::None }
| "||" term simpexpr.operations { $$ = Value::None }
;

term:
  factor term.operations { $$ = Value::None }
;

term.operations:
  %empty { $$ = Value::None }
| '*' factor term.operations { $$ = Value::None }
| '/' factor term.operations { $$ = Value::None }
| "&&" factor term.operations { $$ = Value::None }
;

factor:
  CONST_INT { $$ = Value::None }
| CONST_FLOAT { $$ = Value::None }
| CONST_BOOLEAN { $$ = Value::None }
| functioncall { $$ = Value::None }
| id { $$ = Value::None } 
| '(' assignment ')' { $$ = Value::None }
;

type:
  KW_BOOLEAN { $$ = Value::None; }
| KW_FLOAT { $$ = Value::None }
| KW_INT { $$ = Value::None }
| KW_VOID { $$ = Value::None }
;

id: ID { $$ = Value::None; };
%%

impl Parser {
    /// "Sucess" status-code of the parser
    pub const ACCEPTED: i32 = -1;

    /// "Failure" status-code of the parser
    pub const ABORTED: i32 = -2;

    /// Constructor
    pub fn new(lexer: Lexer) -> Self {
        // This statement was added to manually remove a dead code warning for 'owned_value_at' which is auto-generated code
        Self::remove_dead_code_warning();
        Self {
            yy_error_verbose: true,
            yynerrs: 0,
            debug: false,
            yyerrstatus_: 0,
            yylexer: lexer,
            errors: Vec::new(),
        }
    }

    /// Wrapper around generated `parse` method that also
    /// extracts the `errors` field and returns it.
    pub fn do_parse(mut self) -> Vec<String> {
        self.parse();
        self.errors
    }

    /// Retrieve the next token from the lexer
    fn next_token(&mut self) -> Token {
        self.yylexer.yylex()
    }

    /// Print a syntax error and add it to the errors field
    fn report_syntax_error(&mut self, stack: &YYStack, yytoken: &SymbolKind, loc: YYLoc) {
        let token_name = yytoken.name();
        let error = format!("Unexpected token {} at {:?}", token_name, loc);
        eprintln!("Stack: {}\nError: {}", stack, error);
        self.errors.push(error);
    }

    /// Helper function that removes a dead code warning, which would otherwise interfere with the correction of a submitted
    /// solution
    fn remove_dead_code_warning() {
    	let mut stack = YYStack::new();
    	let yystate: i32 = 0;
    	let yylval: YYValue = YYValue::new_uninitialized();
    	let yylloc: YYLoc = YYLoc { begin: 0, end: 0 };
        stack.push(yystate, yylval.clone(), yylloc);
    	let _ = stack.owned_value_at(0);
    }
}
